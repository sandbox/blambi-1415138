(function($) {

  /**
   * Our dialog object. Can be used to open a dialog to anywhere.
   */
  Drupal.wysiwygMediaProperty = {
    dialog_open: false,
    open_dialog: null
  }

  Drupal.wysiwygMediaProperty.invokeCallback = function(callback) {
    $.post(callback);
  }


  /**
   * Open a dialog window.
   * @param string href the link to point to.
   */
  Drupal.wysiwygMediaProperty.open = function(href, title) {
    if (!this.dialog_open) {
      // Add render references dialog, so that we know that we should be in a
      // dialog.
      href += "?render=wysiwyg-media-property-dialog";
      // Get the current window size and do 75% of the width and 90% of the height.
      // @todo Add settings for this so that users can configure this by themselves.
      var window_width = $(window).width() / 100*75;
      var window_height = $(window).height() / 100*90;
      this.open_dialog = $('<iframe class="references-dialog-iframe" src="' + href + '"></iframe>').dialog({
        width: window_width,
        height: window_height,
        modal: true,
        resizable: false,
        position: ["center", 50],
        title: title,
        close: function() { Drupal.wysiwygMediaProperty.dialog_open = false; }
      }).width(window_width-10).height(window_height)
      $(window).bind("resize scroll", function() {
        // Move the dialog the main window moves.
        if (typeof Drupal.wysiwygMediaProperty == "object" && Drupal.wysiwygMediaProperty.open_dialog != null) {
          Drupal.wysiwygMediaProperty.open_dialog.
            dialog("option", "position", ["center", 10]);
          Drupal.wysiwygMediaProperty.setDimensions();
        }
      });
      this.dialog_open = true;
    }
  }

/**
   * Set dimensions of the dialog dependning on the current winow size
   * and scroll position.
   */
  Drupal.wysiwygMediaProperty.setDimensions = function() {
    if (typeof Drupal.wysiwygMediaProperty == "object") {
      var window_width = $(window).width() / 100*75;
      var window_height = $(window).height() / 100*90;
      this.open_dialog.
        dialog("option", "width", window_width).
        dialog("option", "height", window_height).
        width(window_width-10).height(window_height);
    }
  }

  /**
   * Close the dialog and provide an entity id and a title
   * that we can use in various ways.
   */
  Drupal.wysiwygMediaProperty.close = function() {
    this.open_dialog.dialog('close');
    this.open_dialog.dialog('destroy');
    this.open_dialog = null;
    this.dialog_open = false;
    Drupal.wysiwygMediaProperty.invokeCallback(widget_settings.callback_path);

  }

})(jQuery);