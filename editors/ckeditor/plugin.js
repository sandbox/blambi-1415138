(function ($) {
  CKEDITOR.plugins.add('wysiwyg_media_property', {
    requires : [ 'fakeobjects' ],

    init: function(editor) {
      editor.addCommand('mediaproperties', {
        exec: function(editor) {
          element = editor.getSelection().getStartElement();

          var image_url;
          if(element.$.dataset != undefined)
          {
            image_url = element.$.dataset.ckeSavedSrc;
          }
          else
          {
            image_url = element.$.src;
          }
          $.ajax({
            type: 'GET',
            url: '/wysiwyg_media_property',
            data: {
              img: image_url
            },
            success: function(data) {
              Drupal.wysiwygMediaProperty.open('/file/' + data['fid'] + '/edit', data['filename']);
            },
            error: function(jqXHR, textStatus, errorThrown) {
            //                      console.log(jqXHR);
            //                      console.log(textStatus);
            //                      console.log(errorThrown);
            }
          });
        }
      });

      if (editor.addMenuGroup)
      {
        editor.addMenuGroup("MediaGroup", 100);
      }

      if (editor.addMenuItems)
      {
        editor.addMenuItems( {
          mediaproperties: {
            label: 'Media Properties',
            command: 'mediaproperties',
            icon: this.path + 'ugly.png',
            group : 'MediaGroup',
            order : 12

          }
        });
      }

      if (editor.contextMenu)
      {
        editor.contextMenu.addListener(function(element, selection) {
          if(!element || !element.is('img'))
            return null;



          return {
            mediaproperties: CKEDITOR.TRISTATE_ON
          };
        //return null;
        });
      }
    }
  });
})(jQuery);
