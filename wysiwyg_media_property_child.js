(function ($) {
  /**
   * Attach the child dialog behavior to new content.
   */
  Drupal.behaviors.wysiwygMediaPropertyChild = {
    attach: function(context, settings) {
        // Close the dialog by communicating with the parent.
        console.log('child is loaded');
        parent.Drupal.wysiwygMediaProperty.close();
    }
  }
})(jQuery);
